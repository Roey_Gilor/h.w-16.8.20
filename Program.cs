﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;

namespace Xml_16._8._20
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car("Honda", "new", 2020, "black", 1234, 5);
            XmlSerializer serializer = new XmlSerializer(typeof(Car));
            Car.SerializeACar(@"..\..\xmlfile.xml", car);
            Car returnedCar = Car.DesiralizeACar(@"..\..\xmlfile.xml");
            Car[] cars =
            {
                new Car ("Honda", "new", 2020, "black", 1234, 5),
                new Car ("Yondai", "old", 2018, "white", 4321, 4),
                new Car ("Shevrolet", "beautyfull", 2019, "blue", 5678, 5),
                new Car ("Kia", "nice", 2017, "red", 6548, 2),
            };
            Car.SerializeCarsArray(@"..\..\xmlarrayfile.xml", cars);
            Car[] returnedCars = Car.DesiralizeCarsArray(@"..\..\xmlarrayfile.xml");
            Car car1 = new Car(@"..\..\xmlfile.xml");
            bool check = car1.CarCompare(@"..\..\xmlfile.xml");
        }
    }
}
