﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;

namespace Xml_16._8._20
{
    [XmlRoot("Honda XmlRoot")]
    public class Car
    {
        public string Model { get; set; }
        public string Brand { get; set; }
        [XmlElement ("Shana")]
        public int Year { get; set; }
        [XmlAttribute ("Color")]
        public string Color { get; set; }
        public int Codan { get; set; }
        public int NumberOfSeats { get;  set; }
        public Car()
        {

        }
        public Car(string model, string brand, int year, string color, int codan, int numberOfSeats)
        {
            Model = model;
            Brand = brand;    
            Year = year;
            Color = color;
            Codan = codan;
            NumberOfSeats = numberOfSeats;
        }
        public Car(string fileName)
        {
            Car car = DesiralizeACar(fileName);
            Model = car.Model;
            Brand = car.Brand;
            Year = car.Year;
            Color = car.Color;
            Codan = car.Codan;
            NumberOfSeats = car.NumberOfSeats;
        }
        public static void SerializeACar (string fileName, Car car)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Car));
            using (Stream file = new FileStream(fileName, FileMode.Create))
            {
                serializer.Serialize(file, car);
            }
        }
        public static Car DesiralizeACar(string fileName)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Car));
            Car car = null;
            using (Stream file = new FileStream(fileName, FileMode.Open))
            {
                car = (Car)serializer.Deserialize(file);
            }
            return car;
        }
        public static void SerializeCarsArray(string fileName, Car[] cars)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Car[]));
            using (Stream file = new FileStream(fileName, FileMode.Create))
            {
                serializer.Serialize(file, cars);
            }
        }
        public static Car[] DesiralizeCarsArray(string fileName)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Car[]));
            Car[] cars = null;
            using (Stream file = new FileStream(fileName, FileMode.Open))
            {
                cars = (Car[])serializer.Deserialize(file);
            }
            return cars;
        }
        public bool CarCompare (string fileName)
        {
            Car car = DesiralizeACar(fileName);
            if (this.Model != car.Model)
                return false;
            if (this.Brand != car.Brand)
                return false;
            if (this.Year != car.Year)
                return false;
            if (this.Color != car.Color)
                return false;
            if (this.Codan != car.Codan)
                return false;
            if (this.NumberOfSeats != car.NumberOfSeats)
                return false;
            return true;
        }
    }
}
